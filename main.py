import numpy as np
from numpy.typing import NDArray
from typing import Callable, Dict, List, Tuple

Array_Function = Callable[[NDArray], NDArray]
Chain = List[Array_Function]

X = np.random.randn(1,3)
W = np.random.randn(3,1)

def square(x: NDArray) -> NDArray:
    return np.power(x, 2)

def leaky_relu(x: NDArray) -> NDArray:
    return np.maximum(0.2 * x, x)

def deriv(func: Array_Function,
          input_: NDArray,
          delta: float = 0.001) -> NDArray:
    """
    Applying definition of derivative
    """
    return (func(input_ + delta) - func(input_ - delta)) / (2 * delta)

def chain_length_2(chain: Chain,
                   a: NDArray) -> NDArray:
    assert len(chain) == 2, \
            "Length of input chain should be 2"
    f1 = chain[0]
    f2 = chain[1]
    return f2(f1(a))

def sigmoid(x: NDArray) -> NDArray:
    return 1 / (1 + np.exp(-x))

def chain_deriv_2(chain: Chain, input_range: NDArray) -> NDArray:
    """
    The chain rule states:
        (f2(f1(x)))' = f2'(f1(x)) * f1'(x)
    for any given composite function
    """
    assert len(chain) == 2, \
            "Required a Chain object of length 2"
    assert input_range.ndim == 1, \
            "Function requires a 1 dimensional ndarray as input_range"

    f1, f2 = chain[0], chain[1]
    # df1/dx
    df1dx = deriv(f1, input_range)
    # df2/dx(f1(x))
    df2du = deriv(f2, f1(input_range))
    return df1dx * df2du

def chain_deriv_3(chain: Chain,
        input_range: NDArray) -> NDArray:
    """
    Chain rule for 3 nested functions:
        (f3(f2(f1(x))))' = f3'(f2(f1(x))) * f2'(f1(x)) * f1'(x)
    """
    assert len(chain) == 2, \
            "Required a Chain object of length 3"
    assert input_range.ndim == 1, \
            "Function requires a 1 dimensional ndarray as input_range"

    f1, f2, f3 = chain[0], chain[1], chain[2]

    df1dx = deriv(f1, input_range)
    df2dx = deriv(f2, f1(input_range))
    df3dx = deriv(f3, f2(f1(input_range)))

    """
    The final part here could just be df3dx * chain_deriv_2([chain[0], chain[1]], input_range),
    that is, the intended chain rule usage.
    """

    return df3dx * df2dx * df1dx

def multiple_inputs_add(x: NDArray, y: NDArray, sigma: Array_Function) -> float:
    """
    Function with multiple inputs and addition, forward pass.
    """
    assert x.shape == y.shape, "x and y must be of the same shape"
    a = x + y
    return sigma(a)

def multiple_inputs_add_backward(x: NDArray, y: NDArray, sigma: Array_Function) -> Tuple[float, float]:
    # Forward pass
    a = x + y
    # Compute derivatives
    dsda = deriv(sigma, a)
    # One var goes away as it becomes a constant, the other gets derived and becomes 1
    dadx, dady = 1, 1
    return dsda * dadx, dsda * dady

# Excercise
def multiple_inputs_multiply_backward(x: NDArray, y: NDArray, sigma: Array_Function) -> Tuple[float, float]:
    # Forward
    b = x * y 
    # Compute the derivatives
    dsdb = deriv(sigma, b)
    # Go backward i guess?
    dbdx, dbdy = y, x # If i remember correctly :/
    return dsdb * dbdx, dsdb * dbdy

def matmul_forward(X: NDArray, W: NDArray) -> NDArray:
    """
    Computes the forward pass of a matrix multiplication
    """
    assert X.shape[1] == W.shape[0], "Shape mismatch, not valid for a matrix multiplication"

    N = np.dot(X, W)
    return N

def matmul_backward_first(X: NDArray, W: NDArray) -> NDArray:
    """
    Computes the backward pass of a matrix multiplication with respect to the first argument
    N = <some_vector w1x1,...,wnxn>
    X = <some_vector x1,...,xn>
    dNdX = [dndx1,...,dndxn] = [w1, w2, w3] = transposed(W)
    aka "gradient of N with respect to X"
    """
    dNdX = np.transpose(W, (1, 0))
    return dNdX

def matrix_forward_extra(X: NDArray, W: NDArray, sigma: Array_Function) -> NDArray:
    """
    Computes the forward pass of a function involving matrix multiplcation
    and one extra function sigma

    S = F(X, W) = sigma(dot(X, W)) = sigma(x1w1 + ... + xnwn)
    """
    assert X.shape[1] == W.shape[0]

    N = np.dot(X, W)
    S = sigma(N)
    return S

def matrix_function_backward_1(X: NDArray, W: NDArray, sigma: Array_Function) -> NDArray:
    """
    Computes the derivative of our matrix function with respect to the first element
    Given a function sigma: f(X, W) = sigma(v(X, W)) we need to calculate it's derivative
    with respect to X, that is: 

        dfdX = dSdu(v(X, W)) * dvdX(X, W)
        where:  S = sigma
                v = some function with matrix multiplication
                v(X, W) = (w1x1 + ... + wnxn) 
                dvdx(X, W) = W.T (seen previously)

    """

    assert X.shape[1] == W.shape[0]
    v_result = np.dot(X, W)
    # S = sigma(N) the book puts this here, but is left unused
    dSdN = deriv(sigma, v_result)
    dNdX = np.transpose(W, (1, 0))
    return np.dot(dSdN, dNdX)

def test_function_backward():
    def forward_test(ind1, ind2, inc):
        X1 = X.copy()
        X1[ind1, ind2] = X[ind1, ind2] + inc
        return matrix_forward_extra(X1, W, sigmoid)
    expected: NDArray = ((np.round(forward_test(0, 2, 0.01) - forward_test(0, 2, 0), 4)) / 0.01).squeeze().item()
    backwarded = np.round(matrix_function_backward_1(X, W, sigmoid)[0, 2], 2)
    expected = round(expected)
    backwarded = round(backwarded)
    assert expected == backwarded, "Backward pass was computed incorrectly ex: {ex} bw: {bw}".format(ex=expected, bw=backwarded)
    print("[Passed] test_function_backward")
test_function_backward()

def matrix_function_forward_sum(X: NDArray, W: NDArray, sigma: Array_Function) -> float:
    """
    Computes the result of the forward pass of this function with input ndarrays X and W and
    function sigma. (summing is needed at the end in case of complex matrices coming in as inputs,
    this way backward propagating should become easier, but we'll see...)

    [<number>] means and intermdiate result, or input/output
    [X, W] -> (V) - [N] -> (sigma) - [S] -> (sum) -> [L]
    """
    assert X.shape[1] == W.shape[0]
    N = np.dot(X, W)
    S = sigma(N)
    L = np.sum(S)
    return L

def matrix_function_backward_sum_1(X: NDArray, W: NDArray, sigma: Array_Function) -> NDArray:
    """
    Computes derivative of matrix function with a sum with respect to the first matrix input
    dLdX(X, W) = dLdS(S) * dSdN(N) dot dNdX 
        where:  dNdX = W.T similar to a vector version
                dLdS is just ones...
    """
    assert X.shape[1] == W.shape[0]
    N = np.dot(X, W)
    S = sigma(N)
    L = np.sum(S)
    dLdS = np.ones_like(S)
    dSdN = deriv(sigma, N)
    dLdN = dLdS * dSdN # Remains unused since dLdS are just ones...
    dNdX = np.transpose(W, (1, 0))
    dLdX = np.dot(dSdN, dNdX)
    return dLdX

def test_function_backward_sum():
    np.random.seed(190204)
    X = np.random.randn(3, 3)
    W = np.random.randn(3, 2)
    backwarded = matrix_function_backward_sum_1(X, W, sigmoid)
    expected_alteration = round(backwarded[0, 0], 4)
    X1 = X.copy()
    X1[0, 0] += 0.001
    alteration = round((matrix_function_forward_sum(X1, W, sigmoid) - matrix_function_forward_sum(X, W, sigmoid)) / 0.001, 4)
    assert alteration == expected_alteration
    print("[Passed] test_function_backward_sum")
test_function_backward_sum()

def forward_linear_regression(X_batch, y_batch, weights: Dict[str, NDArray]) -> Tuple[float, Dict[str, NDArray]]:
    assert X_batch.shape[0] == y_batch.shape[0]
    assert X_batch.shape[1] == weights["W"].shape[0]
    assert weights["W"].shape[0] == weights["W"].shape[1] == 1

    N = np.dot(X_batch, weights["W"])
    P = N + weights["B"]
    loss = np.mean(np.power(y_batch - P, 2))

    # save info computed on the forward pass
    forward_info = {}
    forward_info["X"] = X_batch
    forward_info["N"] = N
    forward_info["P"] = P
    forward_info["y"] = y_batch
    return loss, forward_info

def loss_gradients(forward_info: Dict[str, NDArray], weights: Dict[str, NDArray]) -> Dict[str, NDArray]:
    dLdP = -2 * (forward_info["y"] - forward_info["P"])
    dPdN = np.ones_like(forward_info["N"])
    dPdB = np.ones_like(weights["B"])
    dLdN = dLdP * dPdN
    dNdW = np.transpose(forward_info["X"], (1, 0))
    dLdW = np.dot(dNdW, dLdN)
    dLdB = (dLdP * dPdB).sum(axis=0)

    loss_gradients = {}
    loss_gradients["W"] = dLdW
    loss_gradients["B"] = dLdB
    return loss_gradients
